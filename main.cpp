#include "temp.hpp"
#include "mraa.hpp"
#include <stdio.h>
#include <iostream>
using namespace std;

#define RED_LED      13
#define GREEN_LED	 12
#define BLUE_LED	 11

int main(void) {
	
	int in;
	float ftemp = 0, ctemp = 0;
	cout << "enter 1 to get temperature, 0 to exit" << endl;
	cin >> in;

	mraa_gpio_context redLED;
	redLED = mraa_gpio_init(RED_LED);
    mraa_gpio_dir(redLED, MRAA_GPIO_OUT);
    
	mraa_gpio_context      greenLED;
	greenLED = mraa_gpio_init(GREEN_LED);
    mraa_gpio_dir(greenLED, MRAA_GPIO_OUT);
	
	mraa_gpio_context blueLED;
	blueLED = mraa_gpio_init(BLUE_LED);
    mraa_gpio_dir(blueLED, MRAA_GPIO_OUT);
	
	while (in != 0) {

		// function call to set up ports, etc.
		getData(ftemp, ctemp);
		
		// prints
		fprintf(stdout, "ftemp : %.5f \nctemp: %.5f\n", ftemp, ctemp);

		// turns on lights depending on how hot it is
		tempDisplay(ftemp, redLED, blueLED, greenLED);

		if (ftemp >= 70) {
			cout << "Today will be warm! If you're going outside, you won't need a jacket." << endl;
		} else if (ftemp >= 60) {
			cout << "It's not too cold, but not too warm. Wear a light jacket." << endl;
		} else if (ftemp >= 50) {
			cout << "You will definitely need a warm coat. Today will be chilly!" << endl;
		} else {
			cout << "Burrrr! So cold!" << endl;
		}
		
		//ask again for user input;
		cout << "enter 1 to get temperature, 0 to exit" << endl;
		cin >> in;
	}

	cout << "Exiting the program. Goodbye!" << endl;
}

#include "dialog.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Dialog w;
    w.setWindowTitle("Weatherinator");
    w.setFixedSize(450,170);
    w.show();

    return a.exec();
}

/*  dialog.cpp
 *  Author: Jayd Hill
 *
 * Reads in temperature data from arduino temp sensor, parses data, and updates dialog window with temperature in C and F.
 * Automatically refreshes until window is closed.
 *
*/

#include "dialog.h"
#include "ui_dialog.h"
#include <QSerialPort>
#include <QSerialPortInfo>
#include <string>
#include <QDebug>
#include <QMessageBox>


Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog){
        ui->setupUi(this);
        //initialize display to dashes
        ui->faren_number->display("-----");
        ui->celsius_number->display("-----");

        arduino = new QSerialPort(this);
        serialBuffer = "";
        parsed_data_c = "";
        parsed_data_f = "";
        f_value = 0.0;

        bool arduino_is_available = true;

        qDebug() << "Number of serial ports:" << QSerialPortInfo::availablePorts().count();

        QSerialPortInfo portToUse;
        foreach (const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts())
            {
                QString s = QObject::tr("Port:") + serialPortInfo.portName() + "\n"
                            + QObject::tr("Location:") + serialPortInfo.systemLocation() + "\n"
                            + QObject::tr("Description:") + serialPortInfo.description() + "\n"
                            + QObject::tr("Manufacturer:") + serialPortInfo.manufacturer() + "\n"
                            + QObject::tr("Serial number:") + serialPortInfo.serialNumber() + "\n"
                            + QObject::tr("Vendor Identifier:") + (serialPortInfo.hasVendorIdentifier() ? QString::number(serialPortInfo.vendorIdentifier(), 16) : QString()) + "\n"
                            + QObject::tr("Product Identifier:") + (serialPortInfo.hasProductIdentifier() ? QString::number(serialPortInfo.productIdentifier(), 16) : QString()) + "\n"
                            + QObject::tr("Busy:") + (serialPortInfo.isBusy() ? QObject::tr("Yes") : QObject::tr("No")) + "\n";

                //is this the edison? checking for correct port~
                if(!serialPortInfo.isBusy() && (serialPortInfo.description().contains("Edison Virtual") || serialPortInfo.manufacturer().contains("Edison Virtual"))) {
                    portToUse = serialPortInfo;
                }

                //qDebug() << s;
            }

        //port validation check
        if(portToUse.isNull() || !portToUse.isValid() )
            {
                qDebug() << "port is not valid:" << portToUse.portName();
            }

        //Open and configure the arduino port if available
        if(arduino_is_available){
            qDebug() << "Found the arduino port!\n";
            arduino->setPortName(portToUse.portName());
            arduino->setBaudRate(QSerialPort::Baud115200);
            arduino->setDataBits(QSerialPort::Data8);
            arduino->setFlowControl(QSerialPort::NoFlowControl);
            arduino->setParity(QSerialPort::NoParity);
            arduino->setStopBits(QSerialPort::OneStop);
            qDebug() << "Ready to read!\n";
            if (arduino->open(QIODevice::ReadOnly)) { // only need to read data in, not out
                    qDebug() << "Connected on" << portToUse.portName();
                } else {
                    qCritical() << "Serial Port error:" << arduino->errorString();

                    qDebug() << tr("Open error");
                }
            readData();
            connect(arduino, SIGNAL(readyRead()), this, SLOT(readData()));
        }
        else{
            qDebug() << "Couldn't find the correct port for the arduino.\n";
            QMessageBox::information(this, "Serial Port Error", "Couldn't open serial port to arduino.");
        }
}

void Dialog::readData(){
    /*
     * readyRead() can bring data back in bits, so readData uses a serial buffer to parse data
     *
     */
    QStringList buffer_split = serialBuffer.split(","); //  split the serialBuffer string, parsing with ',' as the separator

    //  Check to see if there less than 3 tokens in buffer_split.
    //  If so, then one good value is sandwiched between potentially bad values
    if(buffer_split.length() < 3){
        // no parsed value yet so continue accumulating bytes from serial in the buffer.
        serialData = arduino->readAll();
        serialBuffer = serialBuffer + QString::fromStdString(serialData.toStdString());
        serialData.clear();
    }else{
        // the second element of buffer_split is parsed correctly, update the temperature values
        serialBuffer = "";
        //qDebug() << buffer_split << "\n";
        parsed_data_c = buffer_split[1];
        f_value = (9/5.0) * (parsed_data_c.toDouble()) + 32;
        //get farenheit value
        parsed_data_f = QString::number(f_value, 'g', 4);
        qDebug() << "Temperature C: " << parsed_data_c << "\n";
        qDebug() << "Temperature F: " << parsed_data_f << "\n";
        Dialog::updateTemperature(parsed_data_c, parsed_data_f);
    }
}

void Dialog::updateTemperature(QString sensor_reading_c, QString sensor_reading_f)
{
    //  update the values displayed
    ui->celsius_number->display(sensor_reading_c);
    ui->faren_number->display(sensor_reading_f);
}

Dialog::~Dialog(){
    if(arduino->isOpen()){
            arduino->close(); // close the serial port if it's open
    }
    delete ui;
}

#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QSerialPort>
#include <QByteArray>


namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
    void readData();
    void updateTemperature(QString, QString);

private:
    Ui::Dialog *ui;

    QSerialPort *arduino;
    //static const quint16 arduino_vendor_id = 32903;
    //static const quint16 arduino_product_id = 2718;
    //static const quint16 arduino_vendor_id = 8087;
    //static const quint16 arduino_product_id = "a9e";
    QByteArray serialData;
    QString serialBuffer;
    QString parsed_data_c;
    QString parsed_data_f;
    double f_value;
};

#endif // DIALOG_H

/*
 * temp.hpp
 *      Author: Cathy Webster
 */
#include "mraa.hpp"

#ifndef TEMP_H
#define TEMP_H

int getData(float &ftemp, float &ctemp);

void tempDisplay(float &ftemp, mraa_gpio_context, mraa_gpio_context, mraa_gpio_context);

#endif
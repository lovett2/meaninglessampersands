
CXX = g++
CXXFLAGS = -g -std=c++11

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $<

all: main

MAIN_OBJECTS = main.o temp.o lights.o
main: $(MAIN_OBJECTS)
	$(CXX) -o $@ $(MAIN_OBJECTS) -lmraa

# Remove all objects and test- programs (TODO: add your own executables)
clean:
	$(RM) *.o main

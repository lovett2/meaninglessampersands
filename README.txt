PLEASE READ BEFORE RUNNING THE MAKEFILE TO COMPILE!!!!!

Before the program can be compiled, the mraa library must be installed on the Edison board which you wish to run on.
After connecting your board to terminal, copy & paste:
echo "src mraa-upm http://iotdk.intel.com/repos/3.0/intelgalactic-dev/opkg/i586" > /etc/opkg/mraa-upm.conf
opkg update
opkg install mraa

To set up the sensors:
Temperature sensor must be connected to port A0
LED light must be connected to IO Pin 13


#include "mraa.hpp"
#include "temp.hpp"
#include <math.h>

int getData(float &ftemp, float &ctemp) {
     
    mraa::Aio* A0;
 
    // sets up the temp sensor, connected to port A0
    A0 = new mraa::Aio(0);

    int B=3975;
    uint16_t value;
 
    //read ADC value
    value = A0->read();
    
    float ADC=(1023-value)*10000/value;
    ctemp = 1/(log(ADC/10000)/B+1/298.15)-273.15;
    ftemp = ((9*ctemp)/5) + 32;

    return MRAA_SUCCESS;
}

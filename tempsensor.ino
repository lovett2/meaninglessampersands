// Pin is connected at port A0
const int tempPort = A0;
// B-value of the thermistor
const int B = 3975;
void setup()
{
    // serial communication
    Serial.begin(9600);
}
void loop()
{
    // get the analog reading from the sensor
    int analog_val = analogRead(tempPort);
    // Using the conversion equation, get the resistance value of the sensor
    float res_val = (float)(1023-analog_val)*10000/analog_val;
    // Calculate the Celcius value based on the res value.
    float ctemp = 1/(log(res_val/10000)/B+1/298.15)-273.15;
    float ftemp = (ctemp*(9/5))+32;
    // Print the temperature to the serial console.
    Serial.print(ctemp);
    Serial.print(",");
    Serial.flush();
    // To continue reading the next temp, delay, then loop() again
    delay(1000);
}
#include "temp.hpp"
#include "mraa.hpp"
#include <stdio.h>
#include <iostream>
using namespace std;

#define RED_LED      13
#define GREEN_LED	 12
#define BLUE_LED	 11


void tempDisplay(float &ftemp, mraa_gpio_context redLED, mraa_gpio_context blueLED, mraa_gpio_context greenLED) {

	// turns on specific LEDs depending on temperature
	if(ftemp >= 45) {
		mraa_gpio_write(blueLED, 1);
		if(ftemp >= 60) {
			mraa_gpio_write(greenLED, 1);
			if(ftemp >= 75) {
				mraa_gpio_write(redLED, 1);
			}
			else {
				mraa_gpio_write(redLED, 0);
			}
		}
		else {
			mraa_gpio_write(greenLED, 0);
		}
	}
	else {
		mraa_gpio_write(blueLED, 0);
	}
}